OnlineSales project

- clone project with git command
- install npm, bower global
- run 'bower install' and 'npm install' in local directory
- start front-end server with 'npm start'

To use backend (Try to test with ~nguyenp/Shop/ first.It might not work):
- Import database and sample data    
- Edit config.php 
- go to /api/product/some product number or all   

For Guest
- to get session /api/session    (/Shop/session if on metropolia)
- to login POST to /api/login/ json with user and pass    
- to logout GET to /api/logout/    
- to get Product or Category GET to /api/product/id or /api/category/id 
 For Login User
- to get UserOrder GET to /api/order/ (must login before check session to do this)
- to add UserOrder POST to /api/order/ the Order with all the parameter of Order on sql table except OrderId and a parameter "Detail" which is an array of OrderDetail (with all parameter on SQL database)
{
"DeliveryAddress":"DaLaaaat",
"DeliveryDetail":"abcaasf",
"DeliveryDate":"2008-7-04",
"Detail":[{
            "ProductId":2,
            "Quantity":20,
            "Cost":2000}
         ,{
            "ProductId":3,
            "Quantity":10,
            "Cost":1500}]
}



For Admin only
- to get any thing just go to /api/get/something (something can be Order,Product,User)
- to add POST JSON with ALL parameter except Id to /api/add/(this can be Product,Order,User)  For Order must have a Detail parameter with have an array of OrderDetail (This is for admin interface only)
-to post image post in form data to api/image check out how to in http://stackoverflow.com/questions/20487212/angularjs-file-upload-with-php .
-to delete ANY OBJECT(User,Order,OrderDetails,Product,Category)
    DELETE to api/delete/OBJECT (for ex api/delete/Product) the json with {"id":"id you want to delete"}
-to delete ANY OBJECT(User,Order,OrderDetails,Product,Category)
    PUT to api/update/OBJECT (for ex api/update/Product) the json with {"id":"id you want to update",
                                                                         "UnitPrice":"69",
                                                                         "whatever column except id":"whatever value"}
                                                       