angular.module("modelModule", ["ngResource"])
  .factory("Auth", Auth)
  .factory("Category", Category)
  .factory("Order", Order)
  .factory("Product", Product)
  .factory("Session", Session)
  .factory("User", User);

function Auth($resource, DEFAULT_ROOT_PATH) {
  var model = $resource(DEFAULT_ROOT_PATH + "/api/login", null, {
    login: {
      method: "POST"
    },
    logout: {
      method: "GET",
      url: DEFAULT_ROOT_PATH + "/api/logout"
    },
    register: {
      method: "POST",
      url: DEFAULT_ROOT_PATH + "/api/register"
    }
  });
  return model;
}

function Category($resource, DEFAULT_ROOT_PATH) {
  var model = $resource(DEFAULT_ROOT_PATH + "/api/category");
  return model;
}

function Order($resource, DEFAULT_ROOT_PATH) {
  var model = $resource(DEFAULT_ROOT_PATH + "/api/order", null, {
    addOrder: {
      method: "POST"
    },
    viewOrder: {
      method: "GET"
    },
    delete: {
      method: "DELETE",
      url: DEFAULT_ROOT_PATH + "/api/delete/Order"
    }
  });
  return model;
}

function Product($resource, DEFAULT_ROOT_PATH) {
  var model = $resource(DEFAULT_ROOT_PATH + "/api/product/:id", {
    id: "@productId"
  }, {
    getList: {
      method: "GET",
      url: DEFAULT_ROOT_PATH + "/api/product/all",
      isArray: true
    },
    add: {
      method: "POST",
      url: DEFAULT_ROOT_PATH + "/api/add/Product"
    },
    update: {
      method: "PUT",
      url: DEFAULT_ROOT_PATH + "/api/update/Product"
    },
    delete: {
      method: "DELETE",
      url: DEFAULT_ROOT_PATH + "/api/delete/Product"
    },
    getListByCat: {
      method: "GET",
      url: DEFAULT_ROOT_PATH + "/api/productCat/:id",
      params: {
        id: "@catId"
      }
    }
  });
  return model;
}

function Session($resource, DEFAULT_ROOT_PATH) {
  var model = $resource(DEFAULT_ROOT_PATH + "/api/session");
  return model;
}

function User($resource, DEFAULT_ROOT_PATH) {
  var model = $resource(DEFAULT_ROOT_PATH + "/api/user", {
    id: "@userId"
  });
  return model;
}
