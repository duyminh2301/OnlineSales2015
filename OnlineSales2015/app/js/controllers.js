'use strict';

/* Controllers */
var module = angular.module("controllersModule", []);

module
  .controller("AuthModalCtrl", AuthModalCtrl)
  .controller("LoginCtrl", LoginCtrl)
  .controller("RegisterCtrl", RegisterCtrl)
  .controller("OrdersCtrl", OrdersCtrl)
  .controller("ProductCtrl", ProductCtrl)
  .controller("productEditCtrl", productEditCtrl)
  .controller("UserManagementCtrl", UserManagementCtrl)
  .controller("ShopCtrl", ShopCtrl)
  .controller("ShopListCtrl", ShopListCtrl)
  .controller("RootCtrl", RootCtrl)
  .controller("AppCtrl", AppCtrl)
  .controller("CheckoutCtrl", CheckoutCtrl)
  .controller("ProductDetailsCtrl", ProductDetailsCtrl);

function AppCtrl($rootScope, $alert) {
  var vm = this;
  vm.bodyClasses = "default";
  // this'll be called on every state change in the app
  $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
    if (toState.name === "home" || toState.name === "contact") {
      vm.bodyClasses = "hidden-overflow";
    } else {
      vm.bodyClasses = "default";
    }
  });
}

function RootCtrl($alert, $rootScope, Session, sessionPromise, $state, $location) {
  $rootScope.closeAlert = $alert.closeAlert;
  $rootScope.hasCart = false;
  $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams) {
    if (toState.checkAdmin) {
      if (!$rootScope.isAdmin()) {
        event.preventDefault();
        $alert.add("danger", "Access unauthorized!");
        $state.go("home");
      }
    }
    if (toState.checkLoggedIn) {
      if (!$rootScope.isLoggedIn()) {
        event.preventDefault();
        $alert.add("danger", "Access unauthorized!");
        $state.go("home");
      }
    }
  });
  updateSession();

  function updateSession() {
    Session.get(function(result) {
      $rootScope.session = result;
      var path = $location.path();

      if (path === "/orders") {
        if (!$rootScope.isLoggedIn()) {
          $alert.add("danger", "Access unauthorized!");
          $state.go("home");
        }
      }

      if (path === "/management") {
        if (!$rootScope.isAdmin()) {
          $alert.add("danger", "Access unauthorized!");
          $state.go("home");
        }
      }
    });
  }

  $rootScope.isLoggedIn = function() {
    if ($rootScope.session) {
      return !!$rootScope.session.uid;
    }
    return false;
  }

  $rootScope.isAdmin = function() {
    if ($rootScope.session) {
      return $rootScope.session.role === "admin";
    }
    return false;
  }

  $rootScope.$on("updateSession", updateSession);

  $rootScope.goShop = function() {
    $state.transitionTo("shop.list", {}, {
      reload: true,
      inherit: false,
      notify: true
    });
  }

  $rootScope.isInShop = function() {
    return $state.current.name === "shop.list";
  }
}

function AuthModalCtrl($scope, $uibModal, sessionPromise) {
  var modalOptions = {
    login: {
      templateUrl: "app/views/login.html",
      size: "sm",
      controller: LoginCtrl
    },
    register: {
      templateUrl: "app/views/register.html",
      size: "md",
      controller: RegisterCtrl
    }
  };

  $scope.openLoginModal = function() {
    $uibModal.open(modalOptions.login);
  }

  $scope.openRegisterModal = function() {
    $uibModal.open(modalOptions.register);
  }
}

function LoginCtrl($scope, Auth, $alert, $uibModalInstance, $state) {
  $scope.username = "";
  $scope.password = "";

  $scope.login = function() {
    Auth.login({
      username: $scope.username,
      pass: $scope.password
    }, function(res) {
      $alert.add("success", "Logged in successfully");
      $scope.$emit("updateSession");
      $state.go("home");
      $uibModalInstance.close();
    }, function() {
      $alert.add("danger", "Incorrect password or username");
    });
  }
  $scope.cancel = function() {
    $uibModalInstance.close();
  }
}

function RegisterCtrl(Auth, $scope, $alert, $uibModalInstance) {

  $scope.register = function() {
    var userInfo = {
      firstName: $scope.firstName,
      lastName: $scope.lastName,
      email: $scope.email,
      phoneNumber: $scope.phoneNumber,
      pass: $scope.password,
      username: $scope.username
    };
    Auth.register(userInfo, function() {
      $alert.add("success", "Signed up successfully");
      $uibModalInstance.close();
    }, function(error) {
      $alert.add("danger", error.message);
    });
  };
  $scope.cancel = function() {
    $uibModalInstance.close();
  }
}

function UserManagementCtrl($scope, $rootScope, $alert, Auth, $state) {
  $scope.userOptions = [{
    name: "View Orders",
    action: viewOrders
  }, {
    name: "Log out",
    action: logout
  }];

  function logout() {
    Auth.logout(function() {
      $alert.add("success", "You have logged out successfully");
      $scope.$emit("updateSession");
      $state.go("home");
    });
  }

  function viewOrders() {
    $state.go("orders");
  }

  $scope.manageShop = function() {
    $state.go("management");
  }
}

function ProductCtrl($scope, Product, session, $uibModal, $filter, $alert) {
  $scope.product = {};
  $scope.columns = ["ProductId", "ProductName", "CategoryName", "UnitPrice", "Description", "ManuYear", "Picture", "Edit/Remove"];

  Product.getList(function(res) {
    $scope.products = res;
  });

  $scope.delete = function(p) {
    if (confirm("Are you sure to remove the product")) {
      p.id = p.ProductId;
      Product.delete(p, function(data) {
        $scope.products.splice($scope.products.indexOf(p), 1);
        $alert.add("success", "Deleted successfully!");
      }, function(error) {
        $alert.add("danger", error.message);
      });
    }
  };
  $scope.open = function(p, size) {
    var modalInstance = $uibModal.open({
      templateUrl: 'app/views/productEdit.html',
      controller: 'productEditCtrl',
      size: size,
      resolve: {
        item: function() {
          return p;
        }
      }
    });
    modalInstance.result.then(function(selectedObject) {

      if (selectedObject.save == "insert") {
        $scope.products.push(selectedObject);

      } else if (selectedObject.save == "update") {

        p.ProductName = selectedObject.ProductName;
        p.Description = selectedObject.Description;
        p.UnitPrice = selectedObject.UnitPrice;
        p.CategoryName = selectedObject.CategoryName;
        p.ManuYear = selectedObject.ManuYear;
        p.Picture = selectedObject.Picture;
      }
    });
  };

}

function productEditCtrl($rootScope, $scope, $q, $state, $uibModalInstance, item, Product, $alert, Category, $timeout, Upload) {
  var currentCategory = item.CategoryName;
  $scope.isNewCategory = false;
  $scope.newCategory = item.CategoryName;
  $scope.oldCategory = item.CategoryName;
  $scope.product = angular.copy(item);

  Category.query(function(res) {
    $scope.existedCategories = res;
  });

  $scope.cancel = function() {
    $uibModalInstance.dismiss('Close');
  };
  $scope.title = (item.ProductId > 0) ? 'Edit product' : 'Add new product';
  $scope.buttonText = (item.ProductId > 0) ? 'Update Product' : 'Add New Product';
  var original = item;
  $scope.isClean = function() {
    return angular.equals(original, $scope.product);
  }
  $scope.saveProduct = function(product) {
    if ($scope.isNewCategory) {
      product.CategoryName = $scope.newCategory;
    } else {
      product.CategoryName = $scope.oldCategory;
    }
    var pricePattern = /^[+]?\d+([.]\d+)?$/;
    var yearPattern = /^[1-9]([0-9]+)?$/;
    if(!pricePattern.test(product.UnitPrice) || !yearPattern.test(product.ManuYear)){
      $alert.add("danger", "Please check number inputs");
      return;
    }
    var isUpdatedCategory = product.CategoryName != currentCategory;
    // console.log(product.CategoryName, currentCategory, isUpdatedCategory);
    if (product.ProductId > 0) {
      if (!$scope.picFile && isUpdatedCategory) {
        $alert.add("danger", "Changing category requires at least 1 new picture!");
        return;
      }
      /*Update product*/
      product.id = product.ProductId;
      delete product.CategoryId;
      Product.update(product, function(data) {
        if (data.CategoryId) {
          product.CategoryId = data.CategoryId;
        }
        uploadImages(product, true);
        $alert.add("success", "Updated successfully");
        product.save = "update";
        $uibModalInstance.close(product);
      }, function(error) {
        $alert.add("danger", error.message);
      });

    } else {
      if (!$scope.picFile) {
        $alert.add("danger", "Please at least upload 1 picture!");
        return;
      }
      Product.add(product, function(data) {
        if (data.id) {
          product.ProductId = data.id;
        }
        if (data.CategoryId) {
          product.CategoryId = data.CategoryId;
        }
        uploadImages(product, false);
        $alert.add("success", "Added successfully");
        product.save = "insert";
        $uibModalInstance.close(product);
      }, function(error) {
        $alert.add("danger", error.message);
      });
    }
  };



  function uploadImages(product, refreshPage) {
    if (!$scope.picFile) {
      return;
    }
    var successCount = 0;
    var files = $scope.picFile;
    var imageString = "";

    function proceedUpload() {
      var deffered = $q.defer();
      angular.forEach(files, function(file) {
        imageString = (imageString.length === 0) ? file.name : (imageString + "," + file.name);
        file.upload = Upload.upload({
          url: 'api/image',
          data: {
            file: file,
            CategoryId: product.CategoryId
          }
        });

        file.upload.then(function(response) {
            successCount++;
            if (successCount === (files.length)) {
              deffered.resolve();
            }
            $timeout(function() {
              file.result = response.data;
            });
          },
          function(response) {
            deffered.reject(response);
            if (response.status > 0)
              $scope.errorMsg = response.status + ': ' + response.data;
          });
      });
      return deffered.promise;
    }

    proceedUpload().then(function() {
      Product.update({
        id: product.ProductId,
        Picture: imageString
      }, function() {
        product.Picture = imageString.split(",")[0];
        if (refreshPage) {
          $state.transitionTo("management", {}, {
            reload: true,
            inherit: false,
            notify: true
          });
        }
        console.log(product, imageString);
      }, function() {
        $alert.add("danger", "Cannot upload all the picture. Please try again.")
      })
    });
  }
}

function OrdersCtrl($scope, Order, session, $alert) {
  Order.query(function(res) {
    $scope.orders = res;
  });
  $scope.showDetail = function(orderId) {
    if ($scope.active != orderId) {
      $scope.active = orderId;
    } else {
      $scope.active = null;
    }
  };
  $scope.delete = function(o) {
    if (confirm("Are you sure to remove the order")) {
      o.id = o.OrderId;
      Order.delete(o, function(data) {
        $scope.orders.splice($scope.orders.indexOf(o), 1);
        $alert.add("success", "Delete success");
      }, function(error) {
        $alert.add("danger", error.message);
      });
    }
  };
}

function ShopCtrl($scope, Category, session, cartService, cookieService, $uibModal, $alert, $rootScope) {
  Category.query(function(res) {
    $scope.categories = res;
  });

  function activate() {
    $scope.cartList = cartService.getCart();
    $rootScope.hasCart = ($scope.cartList.length > 0) ? true : false;
  }
  activate();
  $scope.$on("updateCart", activate);

  $scope.spliceCart = function(id) {
    cartService.spliceCart(id);
    activate();
  };

  $scope.changeQuantity = function() {
    cookieService.storeCart($scope.cartList);
  }

  $scope.getListByCat = function(id) {
    $scope.$broadcast("getListByCat", id);
  }

  $scope.openCheckoutModal = function() {
    var isAllowed = true;
    if (!$rootScope.isLoggedIn()) {
      $alert.add("danger", "Please login to checkout!");
      return;
    }
    $scope.cartList.forEach(function(item) {
      if (!item.Quantity) {
        isAllowed = false;
      }
    });
    if (!isAllowed) {
      $alert.add("danger", "Please check quantity!");
      return;
    }
    var option = {
      templateUrl: "app/views/checkout.html",
      size: "md",
      controller: CheckoutCtrl,
      resolve: {
        modalData: function() {
          return $scope.cartList;
        }
      }
    };
    $uibModal.open(option);
  };
}

function CheckoutCtrl($scope, modalData, Order, $alert, $uibModalInstance, cartService, $rootScope) {
  if (!modalData) {
    $alert.add("danger", "No items found!");
    $uibModalInstance.close();
  }

  $scope.totalQuantity = $scope.totalPrice = 0;
  $scope.cartList = modalData;

  $scope.cartList.forEach(function(item) {
    $scope.totalQuantity += item.Quantity;
    $scope.totalPrice += item.UnitPrice * item.Quantity;
    item.Cost = item.UnitPrice * item.Quantity;
  });
  $scope.totalPrice = Math.round($scope.totalPrice * 100) / 100;

  $scope.proceed = function() {
    var orderInfo = {
      DeliveryAddress: $scope.recipientAddress,
      DeliveryDetail: $scope.recipientName + "," + $scope.recipientEmail + "," + $scope.recipientPhone,
      DeliveryDate: new Date(),
      Detail: $scope.cartList
    };
    Order.addOrder(orderInfo, function(res) {
      cartService.removeCart();
      $rootScope.$broadcast("updateCart");
      $alert.add("success", "Checked out successfully");
      $uibModalInstance.close();
    }, function() {
      $alert.add("danger", "Failed to check out");
    });
  };
  $scope.cancel = function() {
    $uibModalInstance.close();
  }
}

function ShopListCtrl($scope, Product, cartService, $uibModal, $http, DEFAULT_ROOT_PATH) {

  function getListByCat(event, id) {
    $http.get(DEFAULT_ROOT_PATH + "/api/productCat/" + id).then(function(res) {
      $scope.products = res.data;
      $scope.isInCategory = true;
    });
  }
  $scope.$on("getListByCat", getListByCat);
  activate();

  function activate() {
    Product.getList(function(res) {
      $scope.products = res;
      $scope.isInCategory = false;
    });
  }

  $scope.goBack = function() {
    activate();
  }

  // $scope.addToCart = function(product) {
  //   cartService.addToCart(product);
  //   $scope.$emit("updateCart");
  // };

  $scope.openProductDetails = function(id, picture) {
    var modalOption = {
      templateUrl: "app/views/productDetails.html",
      size: "md",
      controller: ProductDetailsCtrl,
      resolve: {
        modalData: function() {
          return {
            id: id,
            coverImage: picture
          }
        }
      }
    };
    $uibModal.open(modalOption);
  }
}

function ProductDetailsCtrl($scope, $rootScope, $http, DEFAULT_ROOT_PATH, modalData, cartService, $uibModalInstance) {
  $http.get(DEFAULT_ROOT_PATH + "/api/product/" + modalData.id).then(function(res) {
    $scope.selectedProduct = res.data;
    $scope.productSlides = res.data.Picture;
    $scope.productSlides.splice(0, 0, modalData.coverImage);
    $scope.selectedProduct.Picture = modalData.coverImage;
  });

  $scope.addToCart = function(product) {
    cartService.addToCart(product);
    $rootScope.$broadcast("updateCart");
  };

  $scope.cancel = function() {
    $uibModalInstance.close();
  }
}
