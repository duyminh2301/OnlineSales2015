'use strict';

/* Services */
var module = angular.module("servicesModule", []);

module.factory("$alert", alertService);
module.factory("sessionPromise", sessionPromise);
module.service("cartService", cartService)
module.service("cookieService", cookieStorage);

function alertService($rootScope) {
  var alertService = {};

  // create an array of alerts available globally
  $rootScope.alerts = [];

  alertService.add = function(type, msg) {
    $rootScope.alerts.push({
      'type': type,
      'msg': msg
    });
  };

  alertService.closeAlert = function(index) {
    $rootScope.alerts.splice(index, 1);
  };

  return alertService;
}

function sessionPromise($q, Session) {
  var deffered = $q.defer();

  Session.get(function(result) {
    deffered.resolve(result);
  }, function() {
    alert("Cannot connect to back-end server");
  });

  return deffered.promise;
}

function cartService($alert, cookieService) {
  var cartList = cookieService.getCart() || [];
  var addToCart = function(newObj) {
    var isDuplicated = false;
    cartList.forEach(function(product) {
      if (newObj.ProductId === product.ProductId) {
        product.Quantity++;
        isDuplicated = true;
      }
    });
    if (!isDuplicated) {
      newObj.Quantity = 1;
      cartList.push(newObj);
    }
    cookieService.storeCart(cartList);
    $alert.add("info", "Product added to cart successfully!")
  };

  var getCart = function() {
    return cookieService.getCart() || [];
  };

  var spliceCart = function(id) {
    var error = true;
    angular.forEach(cartList, function(product, index) {
      if (id === product.ProductId) {
        cartList.splice(index, 1);
        cookieService.storeCart(cartList);
        $alert.add("success", "Product removed from cart!");
        error = false;
      }
    });
    if (error) {
      $alert.add("danger", "Cannot find product selected");
    }
  }

  var removeCart = function(){
    cartList = [];
    cookieService.removeCart();
  }

  return {
    addToCart: addToCart,
    getCart: getCart,
    spliceCart: spliceCart,
    removeCart: removeCart
  };
}

function cookieStorage($cookies) {
  var cookieStorage = {};
  var expireDate = new Date();
  // expireDate.setHours(expireDate.getHours() + 1);
  // expireDate.setMinutes(expireDate.getMinutes() + 1);

  cookieStorage.storeCart = function(cart) {
    $cookies.putObject("shopCart", cart);
  }

  cookieStorage.getCart = function() {
    return $cookies.getObject("shopCart");
  }

  cookieStorage.removeCart = function(){
    $cookies.putObject("shopCart", []);
  }

  return cookieStorage;
}
