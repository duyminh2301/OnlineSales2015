"use strict";


// Declare app level module which depends on filters, and services
var app = angular.module("OnlineSales", [
  "controllersModule",
  "directivesModule",
  "servicesModule",
  "filtersModule",
  "modelModule",
  "ui.router",
  "ui.bootstrap",
  "ngCookies",
  "angularUtils.directives.dirPagination",
  "ngFileUpload"
]);
app.constant("DEFAULT_ROOT_PATH", "/OnlineSales2015");
app.config(function($stateProvider, $urlRouterProvider) {

  $urlRouterProvider.otherwise("/home");

  $stateProvider
    .state("app", {
      abstract: true,
      templateUrl: "app/views/app.html",
      controller: "RootCtrl",
      resolve: {
        session: "sessionPromise"
      }
    })
    .state("home", {
      parent: "app",
      url: "/home",
      templateUrl: "app/views/home.html",
      resolve: {
        session: "sessionPromise"
      }
    }).state("management", {
      parent: "app",
      url: "/management",
      templateUrl: "app/views/products.html",
      controller: "ProductCtrl",
      resolve: {
        session: "sessionPromise"
      },
      checkAdmin: true
    })
    .state("orders", {
      parent: "app",
      url: "/orders",
      templateUrl: "app/views/orders.html",
      controller: "OrdersCtrl",
      resolve: {
        session: "sessionPromise"
      },
      checkLoggedIn: true
    })
    .state("shop", {
      parent: "app",
      abstract: true,
      url: "/shop",
      templateUrl: "app/views/shop.html",
      controller: "ShopCtrl",
      resolve: {
        session: "sessionPromise"
      }
    })
    .state("shop.list", {
      url: "/list",
      templateUrl: "app/views/shop-list.html",
      controller: "ShopListCtrl",
      resolve: {
        session: "sessionPromise"
      }
    })
    .state("contact", {
      parent: "app",
      url:"/contact",
      templateUrl: "app/views/contact.html"
    });
});

app.config(function(paginationTemplateProvider) {
  paginationTemplateProvider.setPath("app/views/customPagination.html");
});
