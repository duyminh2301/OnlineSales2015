
CREATE TABLE Category (
 CategoryId INT NOT NULL AUTO_INCREMENT,
 CategoryName VARCHAR(20) UNIQUE,
 PRIMARY KEY (CategoryId)
);
LOAD DATA LOCAL INFILE 'category.txt' into table Category fields terminated
by ';' (CategoryName);

CREATE TABLE Products (
 ProductId INT NOT NULL AUTO_INCREMENT,
 ProductName VARCHAR(25) NOT NULL UNIQUE,
 CategoryId INT NOT NULL,
 UnitPrice FLOAT(10),
 Description VARCHAR(130),
 ManuYear INT,
 Picture VARCHAR(60),
 PRIMARY KEY (ProductId)
);
LOAD DATA LOCAL INFILE 'products.txt' into table Products fields terminated
by ';' (ProductName,CategoryId,UnitPrice,Description,ManuYear,Picture);


CREATE TABLE Users (
 UserId INT NOT NULL AUTO_INCREMENT,
 Username VARCHAR(20) NOT NULL UNIQUE,
 Pass CHAR(60),
 Email VARCHAR(30) UNIQUE,
 PhoneNumber VARCHAR(15),
 Role VARCHAR(10),
 FirstName VARCHAR(15),
 LastName VARCHAR(15),
 PRIMARY KEY (UserId)
);
LOAD DATA LOCAL INFILE 'users.txt' into table Users fields terminated by ';' (Username,Pass,Email,PhoneNumber,Role,FirstName,LastName);


CREATE TABLE Orders (
 OrderId INT NOT NULL AUTO_INCREMENT,
 UserId INT NOT NULL,
 DeliveryAddress VARCHAR(40),
 DeliveryDetail VARCHAR(350),
 DeliveryDate DATETIME,
 PRIMARY KEY (OrderId)
);
LOAD DATA LOCAL INFILE 'orders.txt' into table Orders fields terminated
by ';' (UserId,DeliveryAddress,DeliveryDetail,DeliveryDate);



CREATE TABLE OrderDetail (
 OrderId INT NOT NULL,
 ProductId INT NOT NULL,
 Quantity INT,
 Cost FLOAT(10),
 PRIMARY KEY (OrderId,ProductId)
);
LOAD DATA LOCAL INFILE 'orderDetail.txt' into table OrderDetail fields terminated
by ';' (OrderId,ProductId,Quantity,Cost);

ALTER TABLE Products ADD CONSTRAINT FK_Products_0 FOREIGN KEY (CategoryId) REFERENCES Category (CategoryId);


ALTER TABLE Orders ADD CONSTRAINT FK_Orders_0 FOREIGN KEY (UserId) REFERENCES Users (UserId);


ALTER TABLE OrderDetail ADD CONSTRAINT FK_OrderDetail_0 FOREIGN KEY (OrderId) REFERENCES Orders (OrderId) ON DELETE CASCADE;
ALTER TABLE OrderDetail ADD CONSTRAINT FK_OrderDetail_1 FOREIGN KEY (ProductId) REFERENCES Products (ProductId) ON DELETE CASCADE;


