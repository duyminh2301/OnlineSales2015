<?php
class User {
	public static $table_name="Users";
	public static $columns = array('username', 'pass','email','phoneNumber','role', 'firstName', 'lastName');
    public static $lookup_field="UserId";
	public static $join_table=null;
}

class Product{
	public static $table_name="Products";
	public static $columns = array('ProductName', 'CategoryId', 'UnitPrice', 'Description', 'ManuYear','Picture');
	public static $lookup_field="ProductId";
    public static $join_table="Category";
}
class Category {
	public static $table_name="Category";
	public static $columns = array('CategoryName');
	public static $lookup_field="CategoryId";
    public static $join_table=null;
}
class Order {
	public static $table_name="Orders";
	public static $columns = array('UserId','DeliveryAddress','DeliveryDate','DeliveryDetail');
	public static $lookup_field="OrderId";
    public static $join_table=null;
}
class OrderDetail {
	public static $table_name="OrderDetail";
	public static $columns = array('OrderId', 'ProductId','Quantity','Cost');
	public static $lookup_field="OrderId";
    public static $join_table=null;
}
?>