<?php
 	require_once("BaseAPI.inc.php");
	require("DatabaseObject.php");
	require("db.php");
	class API extends BaseAPI {

		public $data = "";

		private $db = NULL;
		private $permission;
		public function __construct(){
			parent::__construct();				// Init parent contructor
			$this->db =new DBHelper();			// Initiate Database
		}

		/*
		 *  Connect to Database
		*/

		public function processApi(){
			$request = explode('/', $_REQUEST['PATH_INFO']);
			$func = $request[0];

			$this->setPrivilege();


			if(in_array($func,$this->permission)){

				switch($func){
					case "session":
					case "login":
					case "logout":
					case "register":
					case "category":
					case "image":
					case "order":
                    case "test":
                    case "test2":
						$this->$func();
						break;
					case "product":
					case "productCat":
                    case "update":
                    case "delete":
					case "add":
						$this->$func($request[1]);
						break;

					case "get":
						$this->$func($request[1],$request[2]);
						break;
					default:
						$this->response('',404);
					}
			}else{
				$this->response('',404);
			}

/*			if((int)method_exists($this,$func) > 0)
				$this->$func($request[1],$request[2]);
			else
				$this->response('',404); // If the method not exist with in this class "Page not found".
*/
		}
		private function session(){
			if($this->get_request_method() != "GET"){
				$this->response('',406);
			}
			$session = $this->db->getSession();

			$this->response($this->json($session), 200);
		}
		private function login(){
			if($this->get_request_method() != "POST"){
				$this->response('',406);
			}
		 	$username = $this->_json_request['username'];
			$password = $this->_json_request['pass'];

			if(!empty($username) and !empty($password)){
					$query=$this->db->get_sql("*","Users",null,"username='$username'",null,null,null);
					$user = $this->db->query($query);
					$user =!is_null($user) ? array_shift($user) : null;

					require("hash.php");
					if($user!=null&&passwordHash::check_password($user['Pass'],$password)){
						unset($user["Pass"]);
						$user['status']="success";
						$user['message']="Logged in successfully.";
						if (!isset($_SESSION)) {
          					  session_start();
       					 }
					$_SESSION["uid"] = $user["UserId"];
       				$_SESSION['email'] = $user['Email'];
        			$_SESSION['name'] = $user['Username'];
					$_SESSION['role'] = $user['Role'];

					$this->response($this->json($user), 200);
					}
					$response['status'] = "error";
            		$response['message'] = 'Login failed. Incorrect credentials';
					$this->response($this->json($response), 406);

			}
			$error = array('status' => "Failed", "msg" => "Invalid Username or Password");
			$this->response($this->json($error), 400);
		}
	private function logout(){
			if($this->get_request_method() != "GET"){
				$this->response('',406);
			}
			$session = $this->db->destroySession();
   			$response["status"] = "info";
    		$response["message"] = "Logged out successfully";
			$this->response($this->json($response), 200);
		}


	private function register(){
		if($this->get_request_method() != "POST"){
				$this->response('',406);
			}
		$response = array();
		$username=$this->_json_request['username'];
		$email=$this->_json_request['email'];
		$request_password=$this->_json_request["pass"];
		$this->_json_request['role']="customer";

 		$query=$this->db->get_sql("*","Users",null,"username='$username' or email='$email'",null,null,null);
		$isUserExists = $this->db->query($query);

		if(!$isUserExists){
			require("hash.php");
			$this->_json_request["pass"]=passwordHash::hash($request_password);
			$result = $this->db->insert_to_table($this->_json_request, User::$columns, User::$table_name);
			if ($result != NULL) {
				$response["status"] = "success";
				$response["message"] = "User account created successfully";
				$response["uid"] = $result;
				if (!isset($_SESSION)) {
					session_start();
				}
				$_SESSION['uid'] = $response["uid"];
				$_SESSION['role'] = "customer";
				$_SESSION['name'] = $username;
				$_SESSION['email'] = $email;
				$this->response($this->json($response), 200);
			} else {
				$response["status"] = "error";
				$response["message"] = "Failed to create customer. Please try again";
				$this->response($this->json($response), 201);
			}
		}else{
			$response["status"] = "error";
			$response["message"] = "Customer already exist!";
			$this->response($this->json($response), 201);
		}
	}
	private function product($param){
			$this->get("Product",$param);
		}
	private function productCat($param){
		$category_id=$param;
			$sql=$this->db->get_sql("*",Product::$table_name,null,"CategoryId='$category_id'",null,null,null);
			$result=$this->db->query($sql);
			if($result!=null){
				for($i=0;$i<count($result);$i++){
						$images=explode(",",str_replace("\r", '', $result[$i]["Picture"]));

						$result[$i]["Picture"]=array_shift($images);
				}
				$this->response($this->json($result), 200);
			}
				$this->response('', 204);	// If no records "No Content" status
	}
	private function category(){
			$this->get("Category","all");
		}

	private function get($object,$param){
			if(property_exists($object,"lookup_field")){
			$result=null;
			if(is_numeric($param)){
				$result=$this->db->get_by_id($param,$object::$table_name,$object::$join_table,$object::$lookup_field);

				if($object==="Order"){
					$od=$this->getOD($param);
					$result['Detail']=$od;
				}elseif($object==="Product"){
					$images=explode(",",str_replace("\r", '', $result["Picture"]));
					array_shift($images);
					$result["Picture"]=$images;
				}
			}elseif($param==="all"){

				if($object==="Order"){
                    $sql=$this->db->get_sql("OrderId,Username,DeliveryAddress,DeliveryDetail,DeliveryDate",Order::$table_name,User::$table_name,null,null,null,null);
			        $result=$this->db->query($sql);
					for($i=0;$i<count($result);$i++){
					$result[$i]['Detail']=$this->getOD($result[$i]['OrderId']);
					}
				}else{
                    $result=$this->db->get_all($object::$table_name,$object::$join_table);
                }
                if($object==="Product"){
					for($i=0;$i<count($result);$i++){
						$images=explode(",",str_replace("\r", '', $result[$i]["Picture"]))[0];
						$result[$i]["Picture"]=$images;
				    }
				}

			}
			if($result!=null){
				$this->response($this->json($result), 200);
			}
				$this->response('', 204);	// If no records "No Content" status
			}
			$this->response('', 404);
		}

	private function getOD($id){
		$sql=$this->db->get_sql("ProductName,Quantity,Cost",OrderDetail::$table_name,Product::$table_name,"OrderId='$id'",null,null,null);
		$result=$this->db->query($sql);
		return $result;

	}
	public function order(){
		if($this->get_request_method() === "GET"){
            if($_SESSION['role']==="admin"){
                    $this->get("Order","all");
                }else{
				    $this->getUserOrder();
                }
			}elseif($this->get_request_method() === "POST"){

                $this->addUserOrder();

			}else{
                $this->response('', 404);
            }
	}
	private function getUserOrder(){
		$uid=$this->db->getSession()['uid'];
		if(!empty($uid)){
			$sql=$this->db->get_sql("OrderId,Username,DeliveryAddress,DeliveryDetail,DeliveryDate",Order::$table_name,User::$table_name,"UserId='$uid'",null,null,null);
			$result=$this->db->query($sql);
			if(!is_null($result)){
				for($i=0;$i<count($result);$i++){
					$result[$i]['Detail']=$this->getOD($result[$i]['OrderId']);
				}
				$this->response($this->json($result), 200);
			}
			$this->response('', 204);
		}
		$this->response('', 406);
	}
	private function add($object){

			$request_body=$this->_json_request;
            if($object==="Product"){
                $category_id=$this->catNameId($request_body);
                if(is_null($category_id)){
                    $this->response(json_encode( array('status' => "error", "msg" =>"Bad request ")),406);
                }
                unset($request_body["CategoryName"]);
                $request_body["CategoryId"]=$category_id;
            }
			$result=$this->db->insert_to_table($request_body,$object::$columns,$object::$table_name);

			if(!is_null($result)){
				if($object==="Order"){
					for($i=0;$i<count($request_body['Detail']);$i++){
						$request_body['Detail'][$i]["OrderId"]=$result;
						$this->addOD($request_body['Detail'][$i]);
					}
				}
                if($object==="Product"){
                    $success = array('status' => "success", "msg" => $object +" Created Successfully.", "id" => $result,"CategoryId"=>$category_id);
                }else{
                    $success = array('status' => "success", "msg" => $object +" Created Successfully.", "id" => $result);
                }

				$this->response($this->json($success),200);
			}

			$this->response(json_encode( array('status' => "error", "msg" =>"Invalid params")),406);
		}
	private function addOD($request){

			$this->db->insert_to_table($request,OrderDetail::$columns,OrderDetail::$table_name);
		}
	private function addUserOrder(){
		$this->_json_request["UserId"]=$this->db->getSession()["uid"];
		$this->add("Order");
	}


    private function update($object){
        if($this->get_request_method() != "PUT"){
            $this->response('',406);
        }

        $request_body=$this->_json_request;
        if($object==="Product"&&isset($request_body["CategoryName"])&&!isset($request_body["CategoryId"])){

                $category_id=$this->catNameId($request_body);
                if(is_null($category_id)){
                    $this->response(json_encode( array('status' => "error", "msg" =>"Bad request ")),406);
                }
                unset($request_body["CategoryName"]);
                $request_body["CategoryId"]=$category_id;
            }
        $result=$this->db->update_table($request_body,$object::$columns,$object::$lookup_field,$object::$table_name);
        if(!is_null($result)){
            if($object==="Product"&&isset($category_id)){
                $success = array('status' => "success", "msg" => $object +" Updated Successfully.", "Affected row" => $result,"CategoryId" => $category_id);
                }else{
                $success = array('status' => "success", "msg" => $object +" Updated Successfully.", "Affected row" => $result);
                }

                $this->response($this->json($success),201);
            }

        $this->response(json_encode( array('status' => "error", "msg" =>"Invalid id")),406);
}
private function test(){
    $this->response($this->json($this->_request),200);
}
private function test2(){
    $this->response($this->json($this->_json_request),200);
}

private function delete($object){
    if($this->get_request_method() != "DELETE"){
        $this->response('',406);
    }
    $request_body=$this->_json_request;

    //$id =$this->_json_request["id"];
    $id =$this->_request["id"];
    
    if(is_numeric($id)&&$id > 0){
        $result=$this->db->delete_from_table($id,$object::$lookup_field,$object::$table_name);
    }
    if($object==="Product"){
        $catId =$this->_request["CategoryId"];
        $sql=$this->db->get_sql("ProductId",$object::$table_name,null,"CategoryId='$catId'",null,null,null);
	    $noProduct=is_null($this->db->query($sql));
        if($noProduct){
            $this->db->delete_from_table($catId,Category::$lookup_field,Category::$table_name);
        }
    }
    if($result>0){
        $success = array('status' => "success", "msg" => $object +" Deleted Successfully.", "row affected" => $result);
        $this->response($this->json($success),200);
    }

    $this->response(json_encode( array('status' => "error", "msg" =>"Invalid id")),406);
}
	private function setPrivilege(){
		$guest_func=["session","login","logout","register","product","category","productCat","test","test2"];
		$customer_func=array_merge(["order"],$guest_func);
		$admin_func=array_merge(["add","get","image","delete","update"],$customer_func);
		$role=$this->db->getSession()["role"];
		switch($role){
			case "admin":
				$this->permission=$admin_func;
				break;
			case "customer":
				$this->permission=$customer_func;
				break;
			default:
				$this->permission=$guest_func;

		}
	}
	private function image(){

		if($this->get_request_method() == "POST"&&isset($_FILES['file'])){

            //$category=$this->catNameId($this->_request);
            $category=$this->_request["CategoryId"];
            if(is_null($category)){
                $this->response("", 406);
            }
			$errors= array();
			$file_name = $_FILES['file']['name'];
			$file_size =$_FILES['file']['size'];
			$file_tmp =$_FILES['file']['tmp_name'];
			$file_type=$_FILES['file']['type'];
			$file_ext = strtolower(pathinfo($file_name, PATHINFO_EXTENSION));
			$extensions = array("jpeg","jpg","png");
			if(in_array($file_ext,$extensions )=== false){
			$errors[]="image extension not allowed, please choose a JPEG or PNG file.";
			}
			if($file_size > 2097152){
			$errors[]='File size cannot exceed 2 MB';
			}
			if (strpos($category,'..') !== false) {
   				$errors[]='Category name not allowed';
			}
			$directory="../images/".$category."/";
			if (!file_exists($directory)) {
			if (!mkdir($directory, 0777, true)) {
    			$errors[]='Failed to create directory';
				}
            }
			if(empty($errors)==true){
				move_uploaded_file($file_tmp,$directory.$file_name);
				$response["status"] = "success";
				$response["message"] = $file_name." uploaded file: to ".$directory;
                $response["file_name"]=$file_name;
			    $this->response($this->json($response), 201);
            }else{
				$response["status"] = "error";
				$response["message"] = $errors;
                $this->response($this->json($response), 406);
			}
				}
			$this->response('', 404);
	}
    private function catNameId($request_body){
        $cat_name=$request_body["CategoryName"];
        $sql=$this->db->get_sql("CategoryId",Category::$table_name,null,"CategoryName='$cat_name'",null,null,null);
			    $result=$this->db->query($sql);
                if(is_null($result)){
                    return $this->db->insert_to_table($request_body,Category::$columns,Category::$table_name);
                }else{
                    return array_shift($result)["CategoryId"];
                }
    }
	private function json($data){
			if(is_array($data)){
				return json_encode($data);
			}
		}

	}


	//header('Access-Control-Allow-Origin: *');
	$api = new API;
	$api->processApi();
?>
