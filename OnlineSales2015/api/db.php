<?php

class DBHelper{

	private  $conn;
	
	function DBHelper() 
   {
       require_once 'config.php';
       $this->conn = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
	   if ($this->conn->connect_error) {
		  die("Connection failed: " . $this->conn->connect_error);
          throw new Exception('Database error');
    }
   }
   function get_sql($column,$table1,$table2,$condition,$group,$having,$order){
       $response = array();
       $columns=is_null($column)?" ":"SELECT $column ";
       $table1=is_null($table1)?" ":"FROM $table1 ";
       $table2=is_null($table2)?" ":"NATURAL JOIN $table2 ";
       $condition=is_null($condition)?"":"WHERE $condition ";
       $having=is_null($having)?" ":"HAVING $having ";
       $order=is_null($order)?" ":"ORDER BY $order ";
	   $sql=$columns.$table1.$table2.$condition.$having.$order;
       
	   return $sql;
	   
   }
   function query($sql){

       $response = array();
	   $result = $this->conn->query($sql);
	   if ($result->num_rows > 0) {
   	 		while($row = $result->fetch_assoc()) {
           		$response[]=$row;
    		}
	   } else{

           return null;
       }
      
	   return $response;
	   
   }
	public function get_all($table_name,$table_name2) {
        $table_name2=is_null($table_name2) ? "":" NATURAL JOIN ".$table_name2;
		return $this->query("SELECT * FROM ".$table_name.$table_name2);
  }
  
  public function get_by_id($id,$table_name,$table_name2,$column) {
      $table_name2=is_null($table_name2) ? "":" NATURAL JOIN ".$table_name2;
      $sql="SELECT * FROM ".$table_name.$table_name2." WHERE ".$column."={$id} LIMIT 1";
      $result_array = $this->query($sql);
	  return !empty($result_array) ? array_shift($result_array) : false;
  }
   

    public function insert_to_table($request_body, $column_names, $table_name) {
        
        $o = (array) $request_body;
        $keys = array_keys($o);
        $columns = '';
        $values = '';
        foreach($column_names as $desired_key){ 
           if(!in_array($desired_key, $keys)) {
                $$desired_key = '';
            }else{
                $$desired_key = $o[$desired_key];
            }
            $columns = $columns.$desired_key.',';
            $values = $values."'".$$desired_key."',";
        }
        $query = "INSERT IGNORE INTO ".$table_name."(".trim($columns,',').") VALUES(".trim($values,',').")";

        $r = $this->conn->query($query);

        if ($r) {
            $new_row_id = $this->conn->insert_id;
            return $new_row_id;
            } else {
                return NULL;
        }
    }
    public function delete_from_table($id, $column, $table_name){
        $sql="DELETE FROM ".$table_name." WHERE ".$column."={$id}";

        $this->conn->query($sql);
        return $this->conn->affected_rows;
    }
    public function update_table($request_body, $column_names,$id_column, $table_name){
        $id = (int)$request_body['id'];
        $keys = array_keys($request_body);
        $set_command = '';

        foreach($column_names as $desired_key){ // Check the object received. If key does not exist, insert blank into the array.
            if(!in_array($desired_key, $keys)) {
                $desired_key= "";
                $$desired_key = '';
            }else{
                $$desired_key = $request_body[$desired_key];
            }
            $set_command =$desired_key?$set_command.$desired_key."='".$$desired_key."',":$set_command;
        }
        $query = "UPDATE ".$table_name." SET ".trim($set_command,',')." WHERE $id_column=$id";

        $r = $this->conn->query($query);
        if ($r) {
            return $this->conn->affected_rows;
            } else {
                return NULL;
        }
    }
    public function getSession(){
        if (!isset($_SESSION)) {
            session_start();
        }
        $sess = array();
        if(isset($_SESSION['uid']))
        {
            $sess["uid"] = $_SESSION['uid'];
            $sess["name"] = $_SESSION['name'];
            $sess["email"] = $_SESSION['email'];
            $sess["role"] = $_SESSION['role'];
        }
        else
        {
            $sess["uid"] = '';
            $sess["name"] = 'Guest';
            $sess["email"] = '';
            $sess["role"] = '';
        }
        return $sess;
    }
    public function destroySession(){
        if (!isset($_SESSION)) {
        session_start();
        }
        if(isSet($_SESSION['uid']))
        {
            unset($_SESSION['uid']);
            unset($_SESSION['name']);
            unset($_SESSION['email']);
            unset($_SESSION['role']);
            $info='info';
    /*        if(isSet($_COOKIE[$info]))
            {
                setcookie ($info, '', time() - $cookie_time);
            }*/
            $msg="Logged Out Successfully...";
        }
        else
        {
            $msg = "Not logged in...";
        }
        return $msg;
    }

}

?> 